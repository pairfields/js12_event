// 1. Реализовать функционал, что после заполнения формы и нажатия кнопки 'Подтвердить' - новый фильм добавляется в список. Страница не должна перезагружаться.
// Новый фильм должен добавляться в movieDB.movies.
// Для получения доступа к значению input - обращаемся к нему как input.value;

// P.S. Здесь есть несколько вариантов решения задачи, принимается любой, но рабочий.
// 2. Если название фильма больше, чем 21 символ - обрезать его и добавить три точки
// 3. При клике на мусорную корзину - элемент будет удаляться из списка (сложно)
// 4. Если в форме стоит галочка 'Сделать любимым' - в консоль вывести сообщение: 'Добавляем любимый фильм'
// 5. Фильмы должны быть отсортированы по алфавиту



let personalMovieDB = {
  count: null,
  movies: {},
  actors: {},
  genres: [],
  private: false,
  // метод limit проверяет чтоб строка не была пустой, null или больше 50 символов
  // и отдает соответствующее булевое значение
  limit: function (string) {
    if (string == "" || string == null) {
      return true;
    } else {
      return false;
    }
  },
	addToMovies: function(values){
		if (this.limit(values[0].value) || this.limit(values[1].value)) {
			return
		}
		if(values[0].value.length >21){
			if(favorite.checked){
				console.log('Добавляем любимый фильм')
			}
			values[0].value = values[0].value.substr(0,21) + '...';
			this.movies[values[0].value] = values[1].value;
			printFilm(values[0].value, values[1].value);
		}else{
			if(favorite.checked){
				console.log('Добавляем любимый фильм')
			}
			this.movies[values[0].value] = values[1].value;
			printFilm(values[0].value, values[1].value);
		}
},
deleteMovie: function(movieName){
	delete this.movies[movieName]
}
};

document.querySelector('.add').onclick = addFilm;


function addFilm(){
	const inputValues = document.querySelectorAll('.input');
	const favorite = document.getElementById('favorite');

	personalMovieDB.addToMovies(inputValues, favorite);
	favorite.checked = false;
	inputValues[0].value = "";
	inputValues[1].value = "";
	console.log(personalMovieDB)
}

function deleteFilm(e){
	const parent = e.target.parentNode.parentNode;

	personalMovieDB.deleteMovie(getFilmName(parent));
	parent.remove();
}

function getFilmName(str){
	return str.firstChild.className.slice(5)
}

function printFilm(filmName, filmRating){
	const filmsInner = 	document.querySelector('.films_inner');
	const anotherFilmWrapper = document.createElement('li');

	anotherFilmWrapper.className = "film-wrapper";
	filmsInner.append(anotherFilmWrapper);

	const anotherFilm = document.createElement('div');

	anotherFilm.className = "film";
	anotherFilm.classList.add(`${filmName}`);
	anotherFilm.textContent = `Название фильма : ${filmName} Оценка : ${filmRating}`;
	filmsInner.lastChild.append(anotherFilm);
	filmsInner.lastChild.insertAdjacentHTML("beforeend", '<button class = "bin" ><img src="resources/bin.png" alt=""></button>'	)

	filmsInner.lastChild.lastChild.onclick = deleteFilm;

}
